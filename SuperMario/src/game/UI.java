package game;

import java.awt.Font;
import java.io.Serializable;
import java.util.ArrayList;

public class UI implements Serializable{

	ArrayList<Sprite> toPaint = new ArrayList<Sprite>();
	Font fuente = Main.w.customFont("SuperMario256.ttf", 22);
	
	int points;
	int dinericos;
	int numvidas;

	String moneytext = ""; 
	Texto t1 = new Texto("texto", 20, 0, 21, 20, "x" + moneytext);

	String lifetext = "";
	Texto t2 = new Texto("", 20, 0, 21, 45, "x" + lifetext);
	
	String pointstext = "";
	Texto t3 = new Texto("", 20, 0, 21, 70, pointstext);
	
	int minscrollx;
	int maxscrollx;
	int minscrolly;
	int maxscrolly;

	Sprite muestra = new Sprite("algo", 2, 3, 22, 23, "coin.gif"); // UI
	
	Box box = new Box(2, 75);

	Sprite vidas = new Sprite("", 2, 26, 22, 46, "iconovidas.png"); // UI

	private static UI ui = null;

	private UI() {
		minscrollx = 0;
		maxscrollx = 0;
		minscrolly = 0;
		maxscrolly = 0;
		
		t1.unscrollable = true;
		t2.unscrollable = true;
		t3.unscrollable = true;
		box.unscrollable = true;
		box.powerup = 'e';
		muestra.unscrollable = true;
		vidas.unscrollable = true;
		numvidas = 3;
		t1.font = fuente;
		t1.textColor = 0x000000;
		t2.font = fuente;
		t2.textColor = 0x000000;
		t3.font = fuente;
		t3.textColor = 0x000000;
		points = 0;
		dinericos = 0;
		toPaint.add(t1);
		toPaint.add(t2);
		toPaint.add(t3);
		toPaint.add(muestra);
		toPaint.add(vidas);
		toPaint.add(box);
	}

	public static UI getUI() {
		if (ui == null) {
			ui = new UI();
			return ui;
		} else {
			return ui;
		}
	}

	public void contadores(int vidas) {


		
		if (dinericos < 10) {
			moneytext = "x0" + Integer.toString(dinericos);
		} else {
			moneytext = "x" + Integer.toString(dinericos);
		}
		t1.path = moneytext;
		
		if(dinericos == 100) {
			dinericos = 0;
			Mario.vidas++;
			contadores(Mario.vidas);
			return;
		}
		numvidas = vidas;
		lifetext = "x" + Integer.toString(numvidas);
		t2.path = lifetext;

		pointstext = "Points x" + Integer.toString(points);
		t3.path = pointstext;
	}

}
