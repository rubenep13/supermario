package game;

public class Power_Up extends Sprite {

	// name:
	// champi
	// flower
	// 1up
	int acc = 0;
	int cdfloat = 50;
	int initialy;
	char dir = 'd';
	boolean floating = false;
	boolean moving = false;
	boolean emerging = true;
	boolean gravity = true;

	public Power_Up(String name, int x1, int y1) {
		super(name, x1, y1, x1 + 25, y1 + 25, "");
		if (name.equals("champi")) {
			changeImage("mushroom.png");
		} else if (name.equals("flower")) {
			changeImage("flower.gif");
			moving = false;
		} else if (name.equals("1up")) {
			changeImage("1up.png");
		}
		initialy = y1;
	}

	public void move(Field f, Mario m) {
		gravity(f);
		floating(m);
	}

	public void gravity(Field f) {
		if (gravity) {
			if (!floating) {
				if (!isGrounded(f)) {
					y1 += acc;
					y2 += acc;
					if (acc < 10) {
						acc++;
					}
				} else {
					acc = 0;
				}
			}
			getGrounded(f);
		}
	}

	public void floating(Mario m) {
		if (floating) {
			x1 = m.x1;
			x2 = x1 + 25;
			y1 = m.y1 - 100;
			y2 = y1 + 25;
			cdfloat--;
			if (cdfloat == 0) {
				floating = false;
				cdfloat = 50;
			}
		}
	}

	public void move(Field f) {
		if (emerging) {
			y1--;
			y2--;
			if(y1 + 25 == initialy) {
				emerging = false;
				gravity = true;
			}
		}else if (moving) {
			if (dir == 'd') {
				x1+=2;
				x2+=2;
			} else {
				x1-=2;
				x2-=2;
			}
		}
		if(isOnColumn(f)) {
			if(dir == 'd') {
				dir = 'a';
			}else {
				dir = 'd';
			}
		}
	}
	
}
