package game;

public class BloqueInterrogante extends Sprite {

	// name es la recompensa
	// coin
	// booster: champi/flor
	// 1up
	// estrella
	// plant

	public BloqueInterrogante(String name, int x1, int y1, boolean invisible, int plants) {
		super(name, x1, y1, x1 + 25, y1 + 25, "interroganteblock.gif");
		terrain = true;
		solid = true;
		this.invisible = invisible;
		this.plants = plants;
		sprite();
	}
	public BloqueInterrogante(int x1, int y1, boolean invisible, boolean willmove, int plants) {
		super("booster", x1, y1, x1 + 25, y1 + 25, "interroganteblock.gif");
		terrain = true;
		solid = true;
		this.invisible = invisible;
		this.willmove = willmove;
		this.plants = plants;
		sprite();
	}
	
	int plants;
	boolean invisible;
	boolean empty = false;
	boolean willmove = false;

	public void sprite() {
		if (invisible) {
			changeImage("invisible.gif");
		}
	}

	public void recompensar() {
		if (!empty) {
			if (name.equals("coin")) {
				Coin moneda = new Coin(x1 + 2, y1 - 21);
				moneda.fromBlock = true;
				Main.currentMap.sprites.add(moneda);

			} else if (name.equals("booster")) {

				if (!Main.m.grande) {

					Power_Up champi = new Power_Up("champi", x1, y1);
					champi.gravity = false;
					champi.moving = willmove;
					Main.currentMap.addPU(Main.currentMap.sprites.indexOf(this)-1, champi);

				} else {

					Power_Up flor = new Power_Up("flower", x1, y1);
					flor.gravity = false;
					Main.currentMap.addPU(Main.currentMap.sprites.indexOf(this)-1, flor);

				}
			} else if (name.equals("1up")) {

				Power_Up oneup = new Power_Up("1up", x1, y1);
				oneup.gravity = false;
				Main.currentMap.addPU(Main.currentMap.sprites.indexOf(this)-1, oneup);

			} else if (name.equals("plant")) {

				LadderPlant plant = new LadderPlant(x1, y1, plants);
				Main.currentMap.sprites.add(Main.currentMap.sprites.indexOf(this)-1, plant);

			}

			if (invisible) {
				invisible = false;
			}
			empty = true;
		}
	}

}
