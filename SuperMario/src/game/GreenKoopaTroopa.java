package game;

public class GreenKoopaTroopa extends KoopaTroopa implements Steppable, FireDamageable{

	public GreenKoopaTroopa(int x1, int y1) {
		super("Koopa Troopa", x1, y1, x1+30, y1+50, "koopa.gif");
	}

	@Override
	public void lateralMove(Field f) {
		if(isStuckOnTerrain(f)) {
			getGrounded(f);
		}
		if (status == 'f') {
			if (dir == 'd') {
				changeImage("greenkoopaR.gif");
				x1 += 1;
				x2 += 1;
			} else {
				changeImage("greenkoopaL.gif");
				x1 -= 1;
				x2 -= 1;
			}
			if(isOnColumn(f)) {
				if(dir == 'd') {
					dir = 'a';
				}else {
					dir = 'd';
				}
			}
		} else {
			if (isMoving) {
				changeImage("greenshellmoving.gif");
				if (dir == 'd') {
					x1 += 7;
					x2 += 7;
				} else {
					x1 -= 7;
					x2 -= 7;
				}
				for (Sprite s : collidesWithField(f)) {
					if (s instanceof ShellDamageable) {
						ShellDamageable sd = (ShellDamageable) s;
						sd.shellDmg();
					} else if (s instanceof BloqueInterrogante) {
						
						BloqueInterrogante b = (BloqueInterrogante) s;
						b.changeImage("emptyblock.png");
						b.recompensar();
					}
				}
				if(isOnColumn(f)) {
					if(dir == 'd') {
						dir = 'a';
					}else {
						dir = 'd';
					}
				}
			} else {
				changeImage("greenshellstill.png");
			}
		}
	}
}