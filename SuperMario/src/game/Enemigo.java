package game;

public abstract class Enemigo extends Personaje{

	public Enemigo(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}
	
	int acc = 0;
	char dir;
	public void move(Field f) {
		lateralMove(f);
		fall(f);
	}
	
	@Override
	public void fall(Field f) {
		if(!isGrounded(f)) {
			y1 += acc;
			y2 += acc;
			if(acc < 10) {
				acc++;
			}
		}else {
			acc = 0;
		}
	}
	
	@Override
	public abstract void lateralMove(Field f);
	public void changeDir(Field f) {
		if (isGrounded(f)) {
			if (dir == 'd') {
				boolean flag = false;
				for (int i = 0; i < Main.sp.size(); i++) {
					if (Main.sp.get(i).collidesWithPoint(x2, y2 + 1)) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					dir = 'a';
				}
			} else {
				boolean flag = false;
				for (int i = 0; i < Main.sp.size(); i++) {
					if (Main.sp.get(i).collidesWithPoint(x1, y2 + 1)) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					dir = 'd';
				}
			} 
		}
	}
}
