package game;

public class Suelo extends Sprite {

	public Suelo(int x1, int y1, int x2, int y2, String path) {
		super("tile", x1, y1, x2, y2, path);
		terrain = true;
		solid = true;
	}
	
	public void suelar() {
		
	}
}
