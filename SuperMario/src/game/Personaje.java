package game;

public abstract class Personaje extends Sprite{

	public Personaje(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}
	
	public abstract void lateralMove(Field f);
	public abstract void fall(Field f);
}
