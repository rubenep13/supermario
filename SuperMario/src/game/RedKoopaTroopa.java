package game;

public class RedKoopaTroopa extends KoopaTroopa implements Steppable, FireDamageable{

	public RedKoopaTroopa(int x1, int y1) {
		super("Koopa Troopa", x1, y1, x1+30, y1+50, "redkoopaL.gif");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void lateralMove(Field f) {
		if(isStuckOnTerrain(f)) {
			getGrounded(f);
		}
		if (status == 'f') {
			if (dir == 'd') {
				changeImage("redkoopaR.gif");
				x1 += 1;
				x2 += 1;
			} else {
				changeImage("redkoopaL.gif");
				x1 -= 1;
				x2 -= 1;
			}
			if(isOnColumn(f)) {
				if(dir == 'd') {
					dir = 'a';
				}else {
					dir = 'd';
				}
			}
			if(status == 'f') {
				changeDir(f);
			}
		} else {
			if (isMoving) {
				changeImage("redshellmoving.gif");
				if (dir == 'd') {
					x1 += 7;
					x2 += 7;
				} else {
					x1 -= 7;
					x2 -= 7;
				}
				for (Sprite s : collidesWithField(f)) {
					if (s instanceof ShellDamageable) {
						ShellDamageable sd = (ShellDamageable) s;
						sd.shellDmg();
					} else if (s instanceof BloqueInterrogante) {
						
						BloqueInterrogante b = (BloqueInterrogante) s;
						b.changeImage("emptyblock.png");
						b.recompensar();
					}
				}
				if(isOnColumn(f)) {
					if(dir == 'd') {
						dir = 'a';
					}else {
						dir = 'd';
					}
				}
			} else {
				changeImage("redshellstill.png");
			}
		}
	}

}
