package game;

import java.io.IOException;

public class Pipeline extends Sprite{

	int level;
	int mx1 = 0;
	int my2 = 0;
	boolean usable;
	char dir;
	
	public Pipeline(int x1, int y2, boolean big, int level, boolean usable, char dir) {
		super("", x1, y2-0, x1+0, y2, "");
		this.level = level;
		this.usable = usable;
		if(big) {
			name = "pipeline";
			x2 = x1 + 59;
			y1 = y2 - 118;
			changeImage("pipeline.png"); 
		}else {
			name = "littlepipeline";
			x2 = x1 + 59;
			y1 = y2 - 80;
			changeImage("littlepipeline.png");
		}
		solid = true;
		terrain = true;
		if(usable) {
			this.dir = dir;
		}
		
		
	}
	
	public Pipeline(int x1, int y1, int x2, int y2, int level, boolean usable, char dir, int mx1, int my2) {
		super("", x1, y1, x2, y2, "exitpipe.png");
		this.level = level;
		this.usable = usable;
		solid = true;
		terrain = true;
		if(usable) {
			this.dir = dir;
		}
		this.mx1 = mx1;
		this.my2 = my2;
	}
	
	public void enter() throws ClassNotFoundException, IOException {
		Main.setLevel(level);
		Main.init(mx1, my2);
	}
}
