package game;

import java.util.ArrayList;

public class Box extends Sprite {

	char powerup; // e: empty m: mushroom f: flower
	ArrayList<Character> hierarchy = new ArrayList<Character>();
	Power_Up pu;

	public Box(int x1, int y1) {
		super("box", x1, y1, x1 + 30, y1 + 30, "EmptyBox.png");
		hierarchy.add('f');
		hierarchy.add('m');
		hierarchy.add('e');
	}

	public void change() {
		if (powerup == 'e') {
			changeImage("EmptyBox.png");
		} else if (powerup == 'm') {
			changeImage("MushroomBox.png");
		} else if (powerup == 'f') {
			changeImage("FlowerBox.png");
		}
	}
	
	public void fill(char pu) {
		if(hierarchy.indexOf(pu) < hierarchy.indexOf(powerup)) {
			powerup = pu;
			change();
		}
	}

	public void release() {
		if(powerup == 'm') {
			pu = new Power_Up("champi", Main.m.x1, Main.m.y1-100);
		}else if (powerup == 'f') {
			pu = new Power_Up("flower", Main.m.x1, Main.m.y1-100);
		}else if (powerup == 'e') {
			return;
		}
		pu.floating = true;
		pu.emerging = false;
		Main.currentMap.addPU(1,pu);
		powerup = 'e';
		change();
	}
}
