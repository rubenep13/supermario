package game;

public interface Steppable {

	public void stepped();
	
}
