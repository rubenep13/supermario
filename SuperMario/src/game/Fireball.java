package game;

public class Fireball extends Sprite {

	public Fireball(int x1, int y1, int x2, int y2) {
		super("fireball", x1, y1, x2, y2, "fireball.gif");
		// TODO Auto-generated constructor stub
	}

	boolean falling = true;
	int jumpdistance = 7;
	int acc = 7;
	char dir;

	/**
	 * Movimiento de la bola de fuego
	 * @param f
	 */
	public void shot(Field f) {
		move(dir, f);
		jump(f);
		checkFireball(f);
	}

	private void checkFireball(Field f) {
		for(Sprite s : collidesWithField(f)) {
			if(s instanceof Enemigo) {
				if ((s instanceof FireDamageable)) {
					FireDamageable fd = (FireDamageable) s;
					fd.fireDamaged();
					delete();
				}
			}else {
				if (!(s instanceof Power_Up) && !(s instanceof Coin)) {
					if (!isGrounded(f)) {
						delete();
					} 
				}
			}
		}
	}

	/**
	 * Movimiento horizontal.
	 * @param dir
	 * @param f
	 */
	public void move(char dir, Field f) {
		
		if (dir == 'd') {
			x1 += 10;
			x2 += 10;
		}
		if (dir == 'a') {
			x1 -= 10;
			x2 -= 10;
		}
	}

	/**
	 * Movimiento vertical.
	 * @param f
	 */
	public void jump(Field f) {
		
		if (falling) {
			y1 += acc;
			y2 += acc;
			if (acc < 7) {
				acc++;
			}
		}
		
		if(this.isGrounded(f)) {
			falling = false;
			jumpdistance = 7;
			acc = 7;
		}
		if (!falling) {
			if (jumpdistance > 0) {
				y1 -= acc;
				y2 -= acc;
				if (acc > 0) {
					acc--;
				}
				jumpdistance--;
			}else {
				falling = true;
			}
		}
	}

}
