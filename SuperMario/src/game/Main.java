package game;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author Rub�n Esteban Prado
 * @version 1.0.2
 *
 */
public class Main {

	/**
	 * Personaje principal
	 */
	static Mario m = new Mario(50, 249);
	static Field f = new Field();
	static Window w = new Window(f);
	static UI ui = UI.getUI();
	private static int level = -1;

	public static int getLevel() {
		return level;
	}

	public static void setLevel(int level) {
		Main.level = level;
	}

	static String name = "Jonesy";
	static boolean scrollable;
	static boolean nuevo = true;
	static boolean menu = false;
	static Mapa currentMap = new Mapa();

	/**
	 * ArrayList para manipular todas las bolas de fuego.
	 */
	static ArrayList<Fireball> lfire = new ArrayList<Fireball>(); // AQUI
	static ArrayList<Sprite> sp = new ArrayList<Sprite>(); // AQUI

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {

		w.changeSize(720, 480);

		name = w.showInputPopup("Please write your name");
		menu();
		init(0, 0);
		while (true) {
			input();
			m.constantFunctions(f, w);
			dibujar(sp);
			spritesMove();
			sp.clear();
			Thread.sleep(20);
		}
	}


	/**
	 * Inicializa el nivel del juego.
	 */
	public static void revive() {
		f.setWindow(w);
		currentMap.clearLists();
		sp.clear();
		clearLists();
		f.resetScroll();
		currentMap.generateMap(level);
		m = new Mario(currentMap.mariox, currentMap.marioy - 50);
		ui.getUI().minscrollx = m.x1;
		ui.getUI().maxscrollx = m.x2;
		ui.getUI().minscrolly = 100;
		ui.getUI().maxscrolly = 385;
		if (level != 11) {
			f.scroll(-m.x1 + 200, 0);
		}
		ui.contadores(m.vidas);
		ui.box.fill('e');
	}

	public static void init(int mx1, int my2) throws IOException, ClassNotFoundException {
		f.setWindow(w);
		currentMap.clearLists();
		sp.clear();
		clearLists();
		f.resetScroll();
		if(!nuevo) {
			FileInputStream fis = new FileInputStream("datasaved.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object o = ois.readObject();
			Save save = (Save) o;
			ois.close();
			ui = save.ui;
			sp = save.sp;
			currentMap = save.map;
			level = save.level;
			currentMap.generateMap(level);
			m = save.m;
			ui.getUI().minscrollx = m.x1;
			ui.getUI().maxscrollx = m.x2;
			ui.getUI().minscrolly = 100;
			ui.getUI().maxscrolly = 385;
			f.scroll(save.scrollx, save.scrolly);
			nuevo = false;
		}else {
			currentMap.generateMap(level);
			if (mx1 == 0 && my2 == 0) {
				m.x1 = currentMap.mariox;
				m.x2 = m.x1 + 35;
				m.y2 = currentMap.marioy;
			} else {
				m.x1 = mx1;
				m.x2 = m.x1 + 35;
				m.y2 = my2;
			}
			if (m.grande) {
				m.y1 = m.y2 - 65;
			} else {
				m.y1 = m.y2 - 50;
			}
			if (level != 11) {
				f.scroll(-m.x1 + 200, 0);
			}
			ui.contadores(m.vidas);
			ui.box.fill('e');
			ui.getUI().minscrollx = m.x1;
			ui.getUI().maxscrollx = m.x2;
			ui.getUI().minscrolly = 100;
			ui.getUI().maxscrolly = 385;
		}
		
		f.draw(sp);	
	}

	private static void clearLists() {
		lfire = new ArrayList<Fireball>();
	}

	/**
	 * Abre el men� inicial del juego, donde la �nica opci�n es Jugar.
	 * 
	 * @throws InterruptedException
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void menu() throws InterruptedException, IOException, ClassNotFoundException {
		boolean menu = true;
		while (menu) {
			f.background = "titlescreen.png";
			Texto playtext = new Texto("", 260, 225, 363, 255, "PLAY");
			Texto lvltext = new Texto("", 252, 280, 378, 310, "LEVEL");
			Texto loadtext = new Texto("", 260, 335, 363, 365, "LOAD");
			Texto rankingtext = new Texto("", 252, 390, 378, 420, "RANKING");
			
			playtext.font = w.customFont("SuperMario256.ttf", 30);
			lvltext.font = w.customFont("SuperMario256.ttf", 30);
			loadtext.font = w.customFont("SuperMario256.ttf", 30);
			rankingtext.font = w.customFont("SuperMario256.ttf", 30);
			
			ArrayList<Sprite> lmenu = new ArrayList<Sprite>();
			lmenu.add(playtext);
			lmenu.add(lvltext);
			lmenu.add(loadtext);
			lmenu.add(rankingtext);
			f.draw(lmenu);
			
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			boolean selectlvl = false;
			boolean ranking = false;
			
			if (lvltext.collidesWithPoint(x, y)) {
				selectlvl = true;
			}
			if(rankingtext.collidesWithPoint(x, y)) {
				ranking = true;
			}
			if (playtext.collidesWithPoint(x, y)) {
				System.out.println("presionaste");
				if(level != -1) {
					menu = false;
				}
			}
			if(loadtext.collidesWithPoint(x, y)) {
				nuevo = false;
				menu = false;
			}
			while(ranking) {
				f.background = "rankingscreen.png";
				lmenu.clear();
				Texto back = new Texto("", 280, 360, 412, 400, "BACK");
				back.font = w.customFont("SuperMario256.ttf", 40);
				lmenu.add(back);
				int x2 = f.getCurrentMouseX();
				int y2 = f.getCurrentMouseY();
				if(back.collidesWithPoint(x2, y2)) {
					ranking = false;
				}
				
				File file = new File("ranking");
				if(file.exists()) {
					Texto nametext = new Texto("", 50, 30, 150, 70, "NAME");
					Texto pointstext = new Texto("", 300, 30, 425, 70, "POINTS");
					Texto datetext = new Texto("", 500, 30, 600, 70, "DATE");
					nametext.font = w.customFont("SuperMario256.ttf", 40);
					pointstext.font = w.customFont("SuperMario256.ttf", 40);
					datetext.font = w.customFont("SuperMario256.ttf", 40);
					lmenu.add(nametext);
					lmenu.add(pointstext);
					lmenu.add(datetext);
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					int cont = 10;
					int alt = 100;
					while(br.ready()) {
						if(cont > 0) {
							String s = br.readLine();
							String[] arr = s.split(",");
							Texto text1 = new Texto("", 20, alt, 60, alt+20, arr[0]);
							Texto text2 = new Texto("", 200, alt, 300, alt+20, arr[1]);
							Texto text3 = new Texto("", 400, alt, 500, alt+20, arr[2]);
							text1.font = w.customFont("SuperMario256.ttf", 20);
							text2.font = w.customFont("SuperMario256.ttf", 20);
							text3.font = w.customFont("SuperMario256.ttf", 20);
							lmenu.add(text1);
							lmenu.add(text2);
							lmenu.add(text3);
							alt += 30;
							cont--;
						}
					}
				}else {
					Texto notexists = new Texto("", 70, 200, 370, 240, "THERE IS NO RANKING YET");
					notexists.font = w.customFont("SuperMario256.ttf", 40);
					lmenu.add(notexists);
				}
				f.draw(lmenu);
				lmenu.clear();
				Thread.sleep(20);
			}
			while (selectlvl) {
				f.background = "titlescreen.png";
				Texto lvl1 = new Texto("", 100, 230, 128, 270, "1");
				lvl1.font = w.customFont("SuperMario256.ttf", 40);
				Texto lvl2 = new Texto("", 150, 230, 178, 270, "2");
				lvl2.font = w.customFont("SuperMario256.ttf", 40);
				Texto back = new Texto("", 290, 300, 412, 340, "BACK");
				back.font = w.customFont("SuperMario256.ttf", 40);
				lmenu.clear();
				lmenu.add(lvl1);
				lmenu.add(lvl2);
				lmenu.add(back);
				f.draw(lmenu);
				int x2 = f.getCurrentMouseX();
				int y2 = f.getCurrentMouseY();
				if (lvl1.collidesWithPoint(x2, y2)) {
					level = 1;
					selectlvl = false;
				} else if (lvl2.collidesWithPoint(x2, y2)) {
					level = 2;
					selectlvl = false;
				} else if (back.collidesWithPoint(x2, y2)) {
					selectlvl = false;
				}
				lmenu.clear();
				Thread.sleep(20);
			}
			lmenu.clear();
			Thread.sleep(20);
		}
	}

	/**
	 * Realiza el movimiento de los enemigos.
	 */
	public static void spritesMove() {
		for (Enemigo e : currentMap.lenemies) {
			if (e instanceof Rock) {
				Rock r = (Rock) e;
				r.move(f);
			} else {
				e.move(f);
			}
		}

		for (int i = 0; i < currentMap.sprites.size(); i++) {
			if (currentMap.sprites.get(i) instanceof LadderPlant) {
				LadderPlant lp = (LadderPlant) currentMap.sprites.get(i);
				lp.move();
			} else if (currentMap.sprites.get(i) instanceof Power_Up) {
				Power_Up pu = (Power_Up) currentMap.sprites.get(i);
				pu.move(f);
			} else if (currentMap.sprites.get(i) instanceof Coin) {
				Coin c = (Coin) currentMap.sprites.get(i);
				if (c.fromBlock) {
					c.move();
					c.vanish();
				}
			}
		}

		for (Fireball fi : lfire) {
			fi.shot(f);
		}

		for (Iterator iterator = currentMap.lenemies.iterator(); iterator.hasNext();) {
			Enemigo e = (Enemigo) iterator.next();
			if (e.delete) {
				iterator.remove();
			}
		}
		
		for (Iterator iterator = currentMap.sprites.iterator(); iterator.hasNext();) {
			Sprite e = (Sprite) iterator.next();
			if (e.delete) {
				iterator.remove();
			}
		}
		
		for (Iterator iterator = sp.iterator(); iterator.hasNext();) {
			Sprite e = (Sprite) iterator.next();
			if (e.delete) {
				iterator.remove();
			}
		}

		for (Power_Up pu : currentMap.lpowerups) {
			pu.move(f, m);
		}

	}

	/**
	 * Convierte las entradas por teclado en sus correspondientes acciones y efectua
	 * el scrolling.
	 * 
	 * @throws InterruptedException
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void input() throws InterruptedException, IOException, ClassNotFoundException {

		if(w.getPressedKeys().contains('g')) {
			Save save = new Save();
			save.level = level;
			save.m = m;
			save.map = currentMap;
			save.sp = sp;
			save.ui = ui;
			save.scrollx = f.getScrollx();
			save.scrolly = f.getScrolly();
			FileOutputStream fos = new FileOutputStream("datasaved.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(save);
			oos.flush();
			oos.close();
			System.out.println("Has guardado la partida");
		}
		if (!m.inPipeline) {
			if (w.getPressedKeys().contains('a')) {
				m.dir = 'a';
				m.moving = true;
				m.lateralMove(f);

			} else if (w.getPressedKeys().contains('d')) {
				m.dir = 'd';
				m.moving = true;
				m.lateralMove(f);
				for (Sprite s : m.collidesWithField(f)) {
					if (s instanceof Pipeline) {
						Pipeline pl = (Pipeline) s;
						if (pl.getCollisionType(m).equals("l") && pl.dir == 'd') {
							if (pl.usable) {
								m.pipedir = 'd';
								m.inPipeline = true;
							}
						}
					}
				}
			}
			if (w.getPressedKeys().isEmpty()) {
				m.moving = false;
				for (Sprite s : m.collidesWithField(f)) {
					if (s instanceof LadderPlant) {
						m.climbing = true;
					}
				}
			}
			if (w.getPressedKeys().contains(' ') && m.isGrounded(f)) {
				m.jump(15, 15);
			}
			if (w.getPressedKeys().contains('p')) {
				if (m.firecd == 30) {
					if (m.mode == 'f') {
						lfire.add(m.disparar());
						m.firecd = 0;
					}
				}
			}
			if (w.getPressedKeys().contains('r')) {
				init(0, 0);
			}
			if (w.getPressedKeys().contains('o')) {
				ui.box.release();
			}
			if (w.getPressedKeys().contains('w')) {
				for (Sprite s : m.collidesWithField(f)) {
					if (s instanceof LadderPlant) {
						m.climbing = true;
						m.moving = true;
						m.verticalMove(f, 'w');
					}
				}
			} else if (w.getPressedKeys().contains('s')) {
				for (Sprite s : m.collidesWithField(f)) {
					if (s instanceof LadderPlant) {
						m.climbing = true;
						m.moving = true;
						m.verticalMove(f, 's');
					} else if (s instanceof Pipeline) {
						Pipeline pl = (Pipeline) s;
						if (pl.getCollisionType(m).equals("u") && pl.dir == 's') {
							if (pl.usable) {
								m.pipedir = 's';
								m.inPipeline = true;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Dibuja los sprites del ArrayList sp.
	 * 
	 * @param sp ArrayList con todos los sprites.
	 */
	public static void dibujar(ArrayList<Sprite> sp) {

		sp.addAll(lfire);
		sp.addAll(ui.toPaint);
		sp.addAll(currentMap.sprites);
		if (!m.delete) {
			sp.add(m);
		}
		sp.addAll(currentMap.lpipelines);
		f.draw(sp);
	}
}