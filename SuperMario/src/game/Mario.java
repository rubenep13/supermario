package game;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

public class Mario extends Personaje{

	public Mario(int x1, int y1) {
		super("Mario", x1, y1, x1 + 35, y1 + 50, "littlemariostandingR.png");
	}

	/**
	 * Las vidads de Mario.
	 */
	static int vidas = 3;
	/**
	 * El estado de Mario: 0 en el suelo, 1 subiendo, 2 cayendo
	 */
	int status;
	/**
	 * Modo de Mario: n: normal, f: flor, s: estrella
	 */
	char mode = 'n'; //
	/**
	 * La distancia que Mario salta.
	 */
	int jumpdistance;
	/**
	 * La velocidad en p�xeles por ciclo a la que Mario se mueve horizontalmente.
	 */
	int velocity = 4;
	/**
	 * La aceleraci�n que incrementa la velocidad a la que Mario se mueve
	 * verticalmente.
	 */
	int acc = 0;
	/**
	 * El sentido de Mario horizontalmente: 'a' Izquierda, 'd' Derecha.
	 */
	char dir;
	/**
	 * Indica si Mario es grande.
	 */
	boolean grande = false;
	/**
	 * Indica si Mario est� saltando
	 */
	boolean jumping = false;

	boolean climbing = false;

	boolean moving = false;

	boolean inPipeline = false;
	
	char pipedir;
	/**
	 * Indica si Mario ha ca�do.
	 */
	boolean hasDied = false;

	int damagecd = 0;

	int firecd = 0;

	boolean unDmg = false;

	int unDmgCount = 0;

	public void constantFunctions(Field f, Window w) throws InterruptedException, ClassNotFoundException, IOException {
		
		if (!inPipeline) {
			cooldowns();
			changeSprite();
			fall(f);
			points();
			unDmg();
		}
		colisions(f, w);
		
	}

	// constant
	public void cooldowns() {
		if (damagecd < 20) {
			damagecd++;
		}

		if (firecd < 30) {
			firecd++;
		}
	}

	public void points() {
		if(this.x2 > Main.ui.points) {
			Main.ui.points = this.x2;
			Main.ui.contadores(vidas);
		}
	}
	
	// constant
	public void unDmg() {
		if (unDmg) {
			if (unDmgCount < 25) {
				unDmgCount++;
			} else {
				unDmgCount = 0;
				unDmg = false;
			}
		}
	}

	public void dmg() {
		if (damagecd == 20) {
			if (grande) {
				if (mode == 'f') {
					mode = 'n';
				} else {
					y1 += 15;
					grande = false;
				}
			} else {
				muerte();
			}
			damagecd = 0;
		}
	}

	// constant
	public void changeSprite() {
		if (!climbing) {
			if (jumping) {
				if (mode == 'f') {
					if (dir == 'd') {
						changeImage("firemariojumpingR.png");
					} else {
						changeImage("firemariojumpingL.png");
					}
				} else {
					if (grande) {
						if (dir == 'd') {
							changeImage("bigmariojumpingR.png");
						} else {
							changeImage("bigmariojumpingL.png");
						}
					} else {
						if (dir == 'd') {
							changeImage("littlemariojumpingR.png");
						} else {
							changeImage("littlemariojumpingL.png");
						}
					}
				}
			} else {
				if (!moving) {
					if (dir == 'd') {
						if (mode == 'f') {
							changeImage("firemariostandingR.PNG");
						} else {
							if (grande) {
								changeImage("bigmariostandingR.PNG");
							} else {
								changeImage("littlemariostandingR.png");
							}
						}
					} else {
						if (mode == 'f') {
							changeImage("firemariostandingL.PNG");
						} else {
							if (grande) {
								changeImage("bigmariostandingL.PNG");
							} else {
								changeImage("littlemariostandingL.png");
							}
						}
					}
				} else {
					if (dir == 'd') {
						if (mode == 'f') {
							this.changeImage("firemarioR.gif");
						} else {
							if (grande) {
								changeImage("bigmarioR.gif");
							} else {
								changeImage("littlemarioR.gif");
							}
						}
					} else {
						if (mode == 'f') {
							changeImage("firemarioL.gif");
						} else {
							if (grande) {
								changeImage("bigmarioL.gif");
							} else {
								changeImage("littlemarioL.gif");
							}
						}
					}
				}
			}
		} else {
			if (grande) {
				if (!moving) {
					changeImage("marioclimbstill.png");
				} else {
					changeImage("marioclimb.gif");
				}
			} else {
				if (!moving) {
					changeImage("littlemarioclimbstill.PNG");
				} else {
					changeImage("littlemarioclimb.gif");
				}
			}
		}
	}

	/**
	 * Movimiento vertical de Mario.
	 * 
	 * @param f
	 */
	@Override
	// constant
	public void fall(Field f) {

		// saltando
		if (status == 1) {
			jumping = true;
			if (jumpdistance > 0) {
				y1 -= acc;
				y2 -= acc;
				if (Main.scrollable) {
					if (y1 < Main.ui.minscrolly) {
						f.scroll(0, acc);
						Main.ui.minscrolly -= acc;
						Main.ui.maxscrolly -= acc;
					}
				}
				if (acc > 0) {
					acc--;
				}
				jumpdistance--;
			} else {
				status = 2;
			}
		}

		// en el suelo
		else if (isGrounded(f)) {
			status = 0;
			acc = 0;
			jumping = false;
			if (!inPipeline) {
				getGrounded(f);
			}

			// cayendo
		} else {
			if (!isGrounded(f)) {
				if (!climbing) {
					y1 += acc;
					y2 += acc;
					if (Main.scrollable) {
						if (y2 > Main.ui.maxscrolly) {
							f.scroll(0, -acc);
							Main.ui.minscrolly += acc;
							Main.ui.maxscrolly += acc;
						}
					}
					if (acc < 15) {
						acc++;
					}
					jumping = true;
				}
			}
		}
		if (isOnCeiling(f)) {
			status = 2;
		}

		if (y2 >= Window.sizeY) {
			muerte();
		}
	}

	private void muerte() {
		// TODO Auto-generated method stub
		delete();
		hasDied = true;
		vidas--;
		UI.getUI().contadores(vidas);
	}

	/**
	 * Comienza un salto.
	 * 
	 * @param distancia
	 * @param aceleracion
	 */
	public void jump(int distancia, int aceleracion) {

		if (status == 0) {
			jumping = true;
			status = 1;
			acc = aceleracion;
			jumpdistance = distancia;
		}
	}

	@Override
	public void lateralMove(Field f) {

		if (!isOnColumn(f)) {
			if (!climbing) {
				if (dir == 'd') {
					x1 += velocity;
					x2 += velocity;
					if (Main.scrollable) {
						if (x1 > Main.ui.maxscrollx) {
							f.scroll(-velocity, 0);
							Main.ui.minscrollx += velocity;
							Main.ui.maxscrollx += velocity;
						}
					}
				} else if (dir == 'a') {
					x1 -= velocity;
					x2 -= velocity;
					if (Main.scrollable) {
						if (x1 < Main.ui.minscrollx) {
							f.scroll(velocity, 0);
							Main.ui.minscrollx -= velocity;
							Main.ui.maxscrollx -= velocity;
						}
					}
				}
			} else {
				if (dir == 'd') {
					x1++;
					x2++;
					if (Main.scrollable) {
						if (x1 > Main.ui.maxscrollx) {
							f.scroll(-1, 0);
							Main.ui.minscrollx += 1;
							Main.ui.maxscrollx += 1;
						}
					}
				} else if (dir == 'a') {
					x1--;
					x2--;
					if (Main.scrollable) {
						if (x1 < Main.ui.minscrollx) {
							f.scroll(1, 0);
							Main.ui.minscrollx -= 1;
							Main.ui.maxscrollx -= 1;
						}
					}
				}
			}
		}

		boolean flag = false;
		for (Sprite s : collidesWithField(f)) {
			if (s instanceof LadderPlant) {
				LadderPlant lp = (LadderPlant) s;
				if (collidesWith(lp)) {
					flag = true;
					break;
				}
			}
		}
		if (!flag) {
			climbing = false;
		}

		if (this.isStuckOnTerrain(f)) {
			getSided(f);
		}
	}

	public void verticalMove(Field f, char dir) {

		if (dir == 'w') {
			if (!isOnCeiling(f)) {
				y1--;
				y2--;
				if (Main.scrollable) {
					if (y1 < Main.ui.minscrolly) {
						f.scroll(0, 1);
						Main.ui.minscrolly -= 1;
						Main.ui.maxscrolly -= 1;
					}
				}
			}
		} else if (dir == 's') {
			if (!isGrounded(f)) {
				y1++;
				y2++;
				if (Main.scrollable) {
					if (y2 > Main.ui.maxscrolly) {
						f.scroll(0, -1);
						Main.ui.minscrolly += 1;
						Main.ui.maxscrolly += 1;
					}
				}
			}
		}
		boolean flag = false;
		for (Sprite s : collidesWithField(f)) {
			if (s instanceof LadderPlant) {
				LadderPlant lp = (LadderPlant) s;
				if (collidesWith(lp)) {
					flag = true;
					break;
				}
			}
		}
		if (!flag) {
			climbing = false;
		}
	}

	public Fireball disparar() {

		Fireball fire = null;
		if (dir == 'd') {
			fire = new Fireball(x2 + 1, y1 + 20, x2 + 21, y1 + 40);
			fire.dir = dir;
		} else if (dir == 'a') {
			fire = new Fireball(x1 - 21, y1 + 20, x1 - 1, y1 + 40);
			fire.dir = dir;
		}
		return fire;
	}
	// constant
	public void colisions(Field f, Window w) throws InterruptedException, ClassNotFoundException, IOException {

		if (hasDied) {
			if (vidas > 0) {
				Main.revive();
			} else {
				Main.sp.clear();
				f.background = "goscreen.png";
				f.draw(Main.sp);
				Thread.sleep(2000);
				System.exit(0);
			}
		}
		

		for (Sprite s : collidesWithField(f)) {
			if (s instanceof BloqueInterrogante && headOn(s)) {
				s.changeImage("emptyblock.png");
				status = 2;
				BloqueInterrogante b = (BloqueInterrogante) s;
				b.recompensar();
			}
			if (s instanceof Enemigo) {
				if (!(s instanceof Boo)) {
					Enemigo en = (Enemigo) s;
					if (stepsOn(en)) {
						if (en instanceof Steppable) {
							Steppable st = (Steppable) en;
							st.stepped();
							status = 0;
							if (w.getPressedKeys().contains(' ')) {
								jump(15, 15);
							} else {
								jump(7, 7);
							}
						} else {
							dmg();
						}
					} else if (collidesWith(en) && !stepsOn(en)) {
						if (!(s instanceof KoopaTroopa)) {
							dmg();
						} else {
							KoopaTroopa k = (KoopaTroopa) en;
							if (k.status == 'f') {
								dmg();
							} else if (k.isMoving) {
								if (!unDmg) {
									dmg();
								}
							} else {
								k.dir = dir;
								unDmg = true;
								k.isMoving = true;
							}
						}
					}
				} else {
					dmg();
				}

			} else if (s instanceof Coin) {

				Coin c = (Coin) s;
				if (!c.fromBlock) {
					c.delete();
					UI.getUI().dinericos++;
					UI.getUI().contadores(vidas);
				}

			} else if (s instanceof Power_Up) {
				Power_Up pu = (Power_Up) s;
				if (!pu.emerging) {
					if (pu.name.equals("champi")) {
						if (!grande) {
							y1 -= 15;
							grande = true;
						} else {
							UI.getUI().box.fill('m');
						}
					} else if (pu.name.equals("flower")) {
						if (mode != 'f') {
							mode = 'f';
							if (!grande) {
								y1 -= 15;
								grande = true;
							}
						} else {
							UI.getUI().box.fill('f');
						}
					} else if (pu.name.equals("1up")) {
						vidas += 1;
						UI.getUI().contadores(vidas);
					}
					pu.delete();
				}
				
			}else if(s instanceof Pipeline) {
				if(inPipeline) {
					if (pipedir == 's') {
						Pipeline pl = (Pipeline) s;
						if (y1 < pl.y1) {
							y1++;
							y2++;
						} else {
							inPipeline = false;
							pl.enter();
						} 
					}else if(pipedir == 'd') {
						Pipeline pl = (Pipeline) s;
						if (x1 < pl.x1) {
							x1++;
							x2++;
							System.out.println("twiwi");
						} else {
							inPipeline = false;
							pl.enter();
						}
					}
				}
			}
		}
	}
}
