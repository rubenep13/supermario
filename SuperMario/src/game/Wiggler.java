package game;

public class Wiggler extends Enemigo implements Steppable, FireDamageable, ShellDamageable{

	//70*39
	public Wiggler(int x1, int y1) {
		super("Wiggler", x1, y1, x1+80, y1+50, "wiggler.gif");
	}
	
	boolean angry;

	@Override
	public void lateralMove(Field f) {
		
		if (dir == 'd') {
			
			if (!angry) {
				x1 += 1;
				x2 += 1;
				changeImage("wigglerR.gif");
			}else {
				x1 += 3;
				x2 += 3;
				changeImage("angrywigglerR.gif");
			}
		}else {
			
			if (!angry) {
				x1 -= 1;
				x2 -= 1;
				changeImage("wigglerL.gif");
			}else {
				x1 -= 3;
				x2 -= 3;
				changeImage("angrywigglerL.gif");
			}
		}
		if(isOnColumn(f) && isGrounded(f)) {
			if(dir == 'd') {
				dir = 'a';
				changeImage("");
			}else {
				dir = 'd';
				changeImage("");
			}
		}
		if(isStuckOnTerrain(f)) {
			getGrounded(f);
		}
		
		changeDir(f);
		
	}

	@Override
	public void stepped() {
		if(!angry) {
			angry = true;
			y1 += 10;
		}
	}

	@Override
	public void fireDamaged() {
		if(!angry) {
			angry = true;
			y1 += 10;
		}
	}

	@Override
	public void shellDmg() {
		delete();
	}

}
