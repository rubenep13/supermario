package game;

public class Rex extends Enemigo implements Steppable, FireDamageable, ShellDamageable{

	public Rex(int x1, int y1) {
		super("Rex", x1, y1, x1 + 35, y1 + 50, "rexL.gif");
		big = true;
	}

	boolean big;

	@Override
	public void lateralMove(Field f) {
		if (dir == 'd') {
			x1 += 1;
			x2 += 1;
			if (big) {
				changeImage("rexR.gif");
			} else {
				changeImage("littlerexR.gif");
			}
		} else {
			x1 -= 1;
			x2 -= 1;
			if (big) {
				changeImage("rexL.gif");
			} else {
				changeImage("littlerexL.gif");
			}
		}
		if (isOnColumn(f) && isGrounded(f)) {
			if (dir == 'd') {
				dir = 'a';
			} else {
				dir = 'd';
			}
		}
		if (isStuckOnTerrain(f)) {
			getGrounded(f);
		}
	}

	@Override
	public void stepped() {
		if (!Main.m.grande) {
			if (big) {
				y1 += 15;
				x1 += 4;
				x2 -= 4;
				big = false;
			} else {
				delete();
			}
		}else {
			delete();
		}
	}

	@Override
	public void fireDamaged() {
		if (big) {
			y1 += 15;
			x1 += 4;
			x2 -= 4;
			big = false;
		} else {
			delete();
		}
	}

	@Override
	public void shellDmg() {
		if (big) {
			y1 += 15;
			x1 += 4;
			x2 -= 4;
			big = false;
		} else {
			delete();
		}
	}
}
