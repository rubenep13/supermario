package game;

public class Boo extends Enemigo {

	public Boo(int x1, int y1) {
		super("Boo", x1, y1, x1 + 30, y1 + 30, "BooBuddyL.gif");
	}

	public void move(Field f) {
		lateralMove(f);
		fall(f);
	}

	@Override
	public void lateralMove(Field f) {
		if (x1 > Main.m.x1) {
			dir = 'a';
		} else {
			dir = 'd';
		}
		if (dir == Main.m.dir) {
			if (dir == 'a') {
				if (x1 > Main.m.x1) {
					x1-=2;
					x2-=2;
					changeImage("BooChaseL.png");
				}
			} else {
				if (x2 < Main.m.x2) {
					x1+=2;
					x2+=2;
					changeImage("BooChaseR.png");
				}
			}
		} else {
			if (dir == 'a') {
				changeImage("BooBuddyL.gif");
			} else {
				changeImage("BooBuddyR.gif");
			}
		}

	}

	@Override
	public void fall(Field f) {
		if (dir == Main.m.dir) {
			if (y2 < Main.m.y2) {
				y1 += 2;
				y2 += 2;
			} else if( y2 > Main.m.y2){
				y1 -= 2;
				y2 -= 2;
			}else {
				
			}
		}

	}

}
