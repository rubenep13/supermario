package game;

import java.io.Serializable;
import java.util.ArrayList;


public class Mapa implements Serializable{

	ArrayList<Sprite> sprites;
	ArrayList<Enemigo> lenemies;
	ArrayList<Power_Up> lpowerups;
	ArrayList<Pipeline> lpipelines;
	int mariox;
	int marioy;
	
	public Mapa() {
		sprites = new ArrayList<Sprite>();
		lenemies = new ArrayList<Enemigo>();
		lpowerups = new ArrayList<Power_Up>();
		lpipelines = new ArrayList<Pipeline>();
	}
	
	public void generateMap(int level) {
		
		if (!lenemies.isEmpty()) {
			sprites.clear();
		}
		if (!lenemies.isEmpty()) {
			lenemies.clear();
		}
		if (!lpowerups.isEmpty()) {
			lpowerups.clear();
		}
		if (!lpipelines.isEmpty()) {
			lpipelines.clear();
		}
		
		if(level == 1) {
			initialize("background1.png", true);
			mariox = 300;
			marioy = 300;
			
			Suelo tile = new Suelo(0, 385, 1950, 480, "bigtile.png");
			sprites.add(tile);
			BloqueInterrogante block1 = new BloqueInterrogante("coin", 400, 270, false, 0);
			sprites.add(block1);
			Block block2 = new Block(487, 270, "block.png");
			sprites.add(block2);
			BloqueInterrogante block3 = new BloqueInterrogante(512, 270, false, true, 0);
			sprites.add(block3);
			Block block4 = new Block(537, 270, "block.png");
			sprites.add(block4);
			BloqueInterrogante block5 = new BloqueInterrogante("coin", 562, 270, false, 0);
			sprites.add(block5);
			Block block6 = new Block(587, 270, "block.png");
			sprites.add(block6);
			BloqueInterrogante block7 = new BloqueInterrogante("coin", 537, 175, false, 0);
			sprites.add(block7);
			Goomba goomba1 = new Goomba(537, 350);
			sprites.add(goomba1);
			BloqueInterrogante block8 = new BloqueInterrogante("1up", 1800, 245, true, 0);
			sprites.add(block8);
			BloqueInterrogante block9 = new BloqueInterrogante("plant", 662, 270, true, 15);
			sprites.add(block9);
			Pipeline pipeline1 = new Pipeline(725, 386, false, 0, false, 'a');
			lpipelines.add(pipeline1);
			Pipeline pipeline2 = new Pipeline(1050, 386, false, 0, false, 'a');
			lpipelines.add(pipeline2);
			Goomba goomba2 = new Goomba(1200, 360);
			sprites.add(goomba2);
			Pipeline pipeline3 = new Pipeline(1325, 385, true, 0, false, 'a');
			lpipelines.add(pipeline3);
			Goomba goomba3 = new Goomba(1400, 360);
			sprites.add(goomba3);
			GreenKoopaTroopa kt1 = new GreenKoopaTroopa(1500, 300);
			sprites.add(kt1);
			Pipeline pipeline4 = new Pipeline(1650, 385, true, 11, true, 's');
			lpipelines.add(pipeline4);
			Suelo tile2 = new Suelo(2050, 385, 2450, 480, "averagetile.png");
			sprites.add(tile2);
			BloqueInterrogante block10 = new BloqueInterrogante(2238, 270, false, true, 0);
			sprites.add(block10);
			Block block11 = new Block(2213, 270, "block.png");
			sprites.add(block11);
			Block block12 = new Block(2263, 270, "block.png");
			sprites.add(block12);
			Block block13 = new Block(2285, 160, "block.png");
			sprites.add(block13);
			Block block14 = new Block(2310, 160, "block.png");
			sprites.add(block14);
			Block block15 = new Block(2335, 160, "block.png");
			sprites.add(block15);
			Block block16 = new Block(2360, 160, "block.png");
			sprites.add(block16);
			Block block17 = new Block(2385, 160, "block.png");
			sprites.add(block17);
			Block block18 = new Block(2410, 160, "block.png");
			sprites.add(block18);
			Block block19 = new Block(2435, 160, "block.png");
			sprites.add(block19);
			Block block20 = new Block(2460, 160, "block.png");
			sprites.add(block20);
			Goomba goomba4 = new Goomba(2250, 130);
			sprites.add(goomba4);
			Goomba goomba5 = new Goomba(2320, 130);
			sprites.add(goomba5);
			Suelo tile3 = new Suelo(2550, 385, 4485, 480, "bigtile.png");
			sprites.add(tile3);
			Block block21 = new Block(2585, 160, "block.png");
			sprites.add(block21);
			Block block22 = new Block(2610, 160, "block.png");
			sprites.add(block22);
			Block block23 = new Block(2635, 160, "block.png");
			sprites.add(block23);
			BloqueInterrogante block24 = new BloqueInterrogante(2660, 160, false, true, 0);
			sprites.add(block24);
			BloqueInterrogante block25 = new BloqueInterrogante("coin", 2660, 270, false, 0);
			sprites.add(block25);
			Block block26 = new Block(2810, 270, "block.png");
			sprites.add(block26);
			Block block27 = new Block(2835, 270, "block.png");
			sprites.add(block27);
			Goomba goomba6 = new Goomba(2770, 350);
			sprites.add(goomba6);
			Goomba goomba7 = new Goomba(2810, 350);
			sprites.add(goomba7);
			RedKoopaTroopa kt2 = new RedKoopaTroopa(2985, 350);
			sprites.add(kt2);
			BloqueInterrogante block28 = new BloqueInterrogante("coin", 2985, 270, false, 0);
			sprites.add(block28);
			BloqueInterrogante block29 = new BloqueInterrogante("coin", 3085, 270, false, 0);
			sprites.add(block29);
			BloqueInterrogante block30 = new BloqueInterrogante("coin", 3185, 270, false, 0);
			sprites.add(block30);
			Goomba goomba8 = new Goomba(3300, 260);
			sprites.add(goomba8);
			Goomba goomba9 = new Goomba(3370, 260);
			sprites.add(goomba9);
			BloqueInterrogante block31 = new BloqueInterrogante("booster", 3085, 160, false, 0);
			sprites.add(block31);
			Block block32 = new Block(3485, 270, "block.png");
			sprites.add(block32);
			Block block33 = new Block(3585, 160, "block.png");
			sprites.add(block33);
			Block block34 = new Block(3610, 160, "block.png");
			sprites.add(block34);
			Block block35 = new Block(3635, 160, "block.png");
			sprites.add(block35);
			Goomba goomba10 = new Goomba(3700, 260);
			sprites.add(goomba10);
			Goomba goomba11 = new Goomba(3770, 260);
			sprites.add(goomba11);
			Block block36 = new Block(3835, 160, "block.png");
			sprites.add(block36);
			BloqueInterrogante block37 = new BloqueInterrogante("coin", 3860, 160, false, 0);
			sprites.add(block37);
			BloqueInterrogante block38 = new BloqueInterrogante("coin", 3885, 160, false, 0);
			sprites.add(block38);
			Block block39 = new Block(3910, 160, "block.png");
			sprites.add(block39);
			Block block40 = new Block(3860, 270, "block.png");
			sprites.add(block40);
			Block block41 = new Block(3885, 270, "block.png");
			sprites.add(block41);
			int cont = 4010;
			for (int i = 0; i < 4; i++) {
				Block blockx1 = new Block(cont, 360, "woodblock.png");
				sprites.add(blockx1);
				if(i > 0) {
					Block blockx2 = new Block(cont, 335, "woodblock.png");
					sprites.add(blockx2);
				}
				if(i > 1) {
					Block blockx3 = new Block(cont, 310, "woodblock.png");
					sprites.add(blockx3);
				}
				if(i > 2) {
					Block blockx4 = new Block(cont, 285, "woodblock.png");
					sprites.add(blockx4);
				}
				cont += 25;
			}
			cont = 4160;
			for (int i = 0; i < 4; i++) {
				Block blockx1 = new Block(cont, 360, "woodblock.png");
				sprites.add(blockx1);
				if(i < 3) {
					Block blockx2 = new Block(cont, 335, "woodblock.png");
					sprites.add(blockx2);
				}
				if(i < 2) {
					Block blockx3 = new Block(cont, 310, "woodblock.png");
					sprites.add(blockx3);
				}
				if(i < 1) {
					Block blockx4 = new Block(cont, 285, "woodblock.png");
					sprites.add(blockx4);
				}
				cont += 25;
			}
			cont = 4360;
			for (int i = 0; i < 5; i++) {
				Block blockx1 = new Block(cont, 360, "woodblock.png");
				sprites.add(blockx1);
				if(i > 0) {
					Block blockx2 = new Block(cont, 335, "woodblock.png");
					sprites.add(blockx2);
				}
				if(i > 1) {
					Block blockx3 = new Block(cont, 310, "woodblock.png");
					sprites.add(blockx3);
				}
				if(i > 2) {
					Block blockx4 = new Block(cont, 285, "woodblock.png");
					sprites.add(blockx4);
				}
				cont += 25;
			}
			
			
			
			
			
		}else if(level == 11) {
			initialize("ugbackground.gif", false);
			mariox = 50;
			marioy = 100;
			Suelo tile = new Suelo(0, 385, 1725, 460, "bigbigtile.png");
			sprites.add(tile);
			int count = 360;
			for (int i = 0; i < 11; i++) {
				Block block = new Block(0, count, "ugblock.png");
				sprites.add(block);
				count -= 25;
			}
			int count2 = 200;
			for (int i = 0; i < 7; i++) {
				Coin c = new Coin(count2, 250);
				Coin c2 = new Coin(count2, 200);
				if(i >= 1 && i <= 5) {
					Coin c3 = new Coin(count2, 150);
					sprites.add(c3);
				}
				sprites.add(c);
				sprites.add(c2);
				count2 += 25;
			}
			int count3 = 185;
			for (int i = 0; i < 8; i++) {
				Block b = new Block(count3, 360, "ugblock.png");
				Block b2 = new Block(count3, 335, "ugblock.png");
				Block b3 = new Block(count3, 310, "ugblock.png");
				sprites.add(b);
				sprites.add(b2);
				sprites.add(b3);
				count3 += 25;
			}
			Pipeline exitpipe = new Pipeline(600, 0, 720, 385, 1, true, 'd', 4000, 200);
			lpipelines.add(exitpipe);
			
		}else if(level == 2) {
			initialize("background1.png", true);
			mariox = 50;
			marioy = 300;
			Texto text = new Texto(null, 100, 50, 1000, 100, "Este nivel es una muestra de todas las mec�nicas implementadas");
			text.unscrollable = true;
			text.font = Main.w.customFont("SuperMario256.ttf", 30);
			Suelo tile1 = new Suelo(1000, 200, 1500, 300, "b84.png");
			Suelo tile2 = new Suelo(50, 300, 1000, 480, "b84.png");
			BloqueInterrogante prueba = new BloqueInterrogante(125, 200, false, true, 0);
			BloqueInterrogante prueba2 = new BloqueInterrogante("booster", 500, 274, false, 0);
			BloqueInterrogante prueba3 = new BloqueInterrogante("coin", 200, 200, false, 0);
			BloqueInterrogante prueba4 = new BloqueInterrogante("plant", 250, 200, false, 10);
			Block block1 = new Block(350, 100, "block.png");
 			//Power_Up flower = new Power_Up("flower", 150, 274);
			Goomba goomba1 = new Goomba(550, 270);
			RedKoopaTroopa koopa1 = new RedKoopaTroopa(400, 250);
			GreenKoopaTroopa koopa2 = new GreenKoopaTroopa(600, 250);
			Boo boo = new Boo(500, 100);
			Coin coin = new Coin(160, 120);
			Rock rock = new Rock(900, 100);
			Rex rex = new Rex(280, 220);
			Wiggler wiggler = new Wiggler(1200, 150);
			
			sprites.add(text);
			sprites.add(tile1);
			sprites.add(tile2);
			sprites.add(prueba);
			sprites.add(prueba2);
			sprites.add(prueba3);
			sprites.add(prueba4);
			sprites.add(block1);
			//sprites.add(flower);
			sprites.add(goomba1);
			sprites.add(koopa1);
			sprites.add(boo);
			sprites.add(coin);
			sprites.add(rock);
			sprites.add(rex);
			sprites.add(koopa2);
			sprites.add(wiggler);
		}
		
		for (Sprite s : sprites) {
			if(s instanceof Enemigo) {
				Enemigo e = (Enemigo) s;
				lenemies.add(e);
			}
		}
	}
	
	public void initialize(String background, boolean scrollable) {
		Main.f.background = background;
		Main.f.resetScroll();
		Main.scrollable = scrollable;
		Main.m.dir = 'd';
	}
	
	public void addPU(int index, Power_Up pu) {
		sprites.add(index, pu);
		lpowerups.add(0, pu);
	}

	public void clearLists() {
		if (!lenemies.isEmpty()) {
			sprites.clear();
		}
		if (!lenemies.isEmpty()) {
			lenemies.clear();
		}
		if (!lpowerups.isEmpty()) {
			lpowerups.clear();
		}
		if (!lpipelines.isEmpty()) {
			lpipelines.clear();
		}
		sprites.clear();
	}
	
}
