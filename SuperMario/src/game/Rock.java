package game;

public class Rock extends Enemigo {

	public Rock(int x1, int y1) {
		super("Rock", x1, y1, x1 + 48, y1 + 64, "Rock.png");

		initialy = y1;
		positionx = x1 + (2 * (x2 - x1));
		terrain = true;
		solid = true;
	}

	boolean falling = false;
	boolean activated = false;
	int initialy;
	int positionx;
	int counter = 0;

	@Override
	public void move(Field f) {
		activate();
		if (activated) {
			if (falling) {
				if (!isGrounded(f)) {
					fall(f);
				} else {
					getGrounded(f);
					if (counter < 50) {
						counter++;
					} else {
						counter = 0;
						falling = false;
					}
					acc = 0;
				}
			} else {
				changeImage("Rock.png");

				if (y1 > initialy) {
					y1--;
					y2--;
				} else {
					activated = false;
					counter = 0;
				}

			}
		}
	}

	public void activate() {
		if (!activated) {
			int mx = Main.m.x1 + (2 * (Main.m.x2 - Main.m.x1));
			int dpos = (positionx - mx);
			if (dpos < 0) {
				dpos = dpos * (-1);
			}
			if (dpos < 75) {
				activated = true;
				falling = true;
			}
		}
	}

	@Override
	public void lateralMove(Field f) {

	}


	@Override
	public void fall(Field f) {
		changeImage("AngryRock.png");
		y1 += acc;
		y2 += acc;
		if (acc < 15) {
			acc++;
		}
	}

}
