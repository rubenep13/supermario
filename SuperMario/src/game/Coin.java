package game;

public class Coin extends Sprite{

	public Coin(int x1, int y1) {
		super("coin", x1, y1, x1+20, y1+20, "coin.gif");
		initialy = y1;
	}
	
	int cd = 25;
	boolean vanish = false;
	boolean fromBlock = false;
	int initialy;
	
	public void vanish() {
		if (vanish) {
			cd--;
			if (cd == 0) {
				UI.getUI().dinericos++;
				UI.getUI().contadores(Main.m.vidas);
				delete();
			} 
		}
	}
	
	public void move() {
		if(y1 > initialy + 20) {
			y1 -= 2;
			y2 -= 2;
		}else {
			vanish = true;
		}
	}
}
