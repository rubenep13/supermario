package game;

public class LadderPlant extends Sprite {

	public LadderPlant(int x1, int y1, int plants) {
		super("plant", x1, y1, x1 + 25, y1 + 25, "ladderplant.png");
		position = plants;
		distancia = 25 * position;
	}

	int position;
	int distancia;
	int count = 0;

	public void move() {
		if (distancia > count) {
			y1--;
			y2--;
			count++;
		}
		if (count == 25) {
			grow();
		}
	}

	public void grow() {

		if (position > 1) {
			LadderPlant plant = new LadderPlant(x1, y1+25, position - 1);
			Main.currentMap.sprites.add(1, plant);
		}
	}

}
