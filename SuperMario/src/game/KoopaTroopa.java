package game;

public abstract class KoopaTroopa extends Enemigo implements Steppable, FireDamageable, ShellDamageable{

	public KoopaTroopa(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}

	char status = 'f'; // full (f) / empty (e)
	boolean isMoving = false;
	
	public void  shellDmg() {
		delete();
	}
	
	public void stepped() {

		if (status == 'f') {
			changeImage("greenshellstill.png");
			status = 'e';
			y1 += 28;
		} else {
			if (isMoving) {
				isMoving = false;
				changeImage("greenshellstill.png");
			} else {
				isMoving = true;
			}
		}
	}
	
	public void fireDamaged() {
		if (status == 'f') {
			changeImage("greenshellstill.png");
			status = 'e';
			y1 += 28;
		}

	}
}
