package game;

public class Goomba extends Enemigo implements Steppable, FireDamageable, ShellDamageable{

	public Goomba(int x1, int y1) {
		super("Goomba", x1, y1, x1+30, y1+30, "galoombaL.gif");
	}

	@Override
	public void lateralMove(Field f) {
		if (dir == 'd') {
			x1 += 1;
			x2 += 1;
			changeImage("galoombaR.gif");
		}else {
			x1 -= 1;
			x2 -= 1;
			changeImage("galoombaL.gif");
		}
		if(isOnColumn(f) && isGrounded(f)) {
			if(dir == 'd') {
				dir = 'a';
				changeImage("galoombaL.gif");
			}else {
				dir = 'd';
				changeImage("galoombaR.gif");
			}
		}
		if(isStuckOnTerrain(f)) {
			getGrounded(f);
		}
		changeDir(f);
	}

	@Override
	public void stepped() {
		delete();
	}

	@Override
	public void fireDamaged() {
		delete();
	}

	@Override
	public void shellDmg() {
		delete();
	}
}
