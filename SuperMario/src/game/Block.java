package game;

public class Block extends Sprite{

	public Block(int x1, int y1, String path) {
		super("block", x1, y1, x1+25, y1+25, path);
		terrain = true;
		solid = true;
	}
	
	

}
